# Backend Test API

RESTful API for money transfers between accounts.

## Getting Started

### Prerequisites

1. [Java 1.8](https://www.java.com/)
2. [Gradle 4.6](https://gradle.org/)

### Installing

Clean project.
```
gradle clean
```

Download dependencies and generate jar

```
gradle fatJar
```

Run API

```
java -jar path/to/project/build/libs/Backend-test-api-1.0.jar
```

## Running the tests

Running unit test.
```
gradle clean test
```
Link to see results: path/to/project/build/reports/tests/test/index.html


Generate file coverage.
```
gradle clean test && gradle jacocoTestReport
```
Link to see results: path/to/project/build/reports/jacoco/test/html/index.html

## Endpoints
### Users
Create a new user
```
curl -H "Content-Type: application/json" -X POST \
-d '{"firstName":"Test", "lastName":"dump", "email":"test@gmail.com", "idNumber":"{idNumber}"}' \
http://$hostname:8080/api/users
```
Get all users
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/users
```
Get user by user id number {idNumber}
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/users/{idNumber}
```
Update user by user id number {idNumber}
```
curl -H "Content-Type: application/json" -X PUT \
-d '{"lastName":"abc", "firstName":"abc"}' \
http://$hostname:8080/api/users/{idNumber}
```
Get all accounts by user id number {idNumber}
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/users/{idNumber}/accounts
```

### Banks
Create a new bank
```
curl -H "Content-Type: application/json" -X POST \
-d '{"name":"Test Bank", "description":"Test Bank", "idNumber":"{idNumber}"}' \
http://$hostname:8080/api/banks
```
Get all banks
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/banks
```
Get bank by bank id number {idNumber}
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/banks/{idNumber}
```
Update bank by bank id number {idNumber}
```
curl -H "Content-Type: application/json" -X PUT \
-d '{"name":"abc", "description":"abc"}' \
http://$hostname:8080/api/banks/{idNumber}
```
Get all accounts by bank id number {idNumber}
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/banks/{idNumber}/accounts
```
Create new account
```
curl -H "Content-Type: application/json" -X POST \
-d '{"name":"Test Account", "type":"CURRENT_ACCOUNT", "currency":"POUNDS_STERLING", "number":"{accountNumber}", "balance":10000}' \
http://$hostname:8080/api/banks/{bankIdNumber}/users/{userIdNumber}/accounts
```

### Accounts
Get account by account number {accountNumber}
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/accounts/{accountNumber}
```
Get transactions by account number {accountNumber}
```
curl -i -H "Accept: application/json" \
-H "Content-type: application/json" \
-X GET http://$hostname:8080/api/accounts/{accountNumber}/transactions
```

### Transaction
Realize a new transaction
```
curl -H "Content-Type: application/json" -X POST \
-d '{"originId":"{originAccount}", "destinationId":"{destinationAccount}", "amount":"200"}' \
http://$hostname:8080/api/transactions
```
