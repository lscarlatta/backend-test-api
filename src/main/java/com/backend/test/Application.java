package com.backend.test;

import com.backend.test.controller.AccountController;
import com.backend.test.controller.BankController;
import com.backend.test.controller.TransactionController;
import com.backend.test.controller.UserController;
import com.backend.test.exception.ExceptionHandler;
import com.backend.test.service.AccountService;
import com.backend.test.service.BankService;
import com.backend.test.service.TransactionService;
import com.backend.test.service.UserService;
import com.backend.test.service.impl.AccountServiceImpl;
import com.backend.test.service.impl.BankServiceImpl;
import com.backend.test.service.impl.TransactionServiceImpl;
import com.backend.test.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import static spark.Spark.after;
import static spark.Spark.path;
import static spark.Spark.port;

public class Application {

    private static final Logger LOGGER = Logger.getLogger(Application.class);

    private static final int PORT = 8080;
    private static final String APPLICATION_JSON = "application/json";

    public static void main(String[] args) {
        LOGGER.info("Starting server");
        port(PORT);

        final UserService userService = new UserServiceImpl();
        final BankService bankService = new BankServiceImpl();
        final AccountService accountService = new AccountServiceImpl();
        accountService.setUserService(userService);
        accountService.setBankService(bankService);

        final TransactionService transactionService = new TransactionServiceImpl();
        transactionService.setAccountService(accountService);

        path("/api", () -> {
            new UserController(userService, accountService);
            new BankController(bankService, accountService);
            new AccountController(accountService, transactionService);
            new TransactionController(transactionService);
        });

        after((request, response) -> response.type(APPLICATION_JSON));

        new ExceptionHandler();

        LOGGER.info(String.format("Server started at port: %s", port()));
    }
}
