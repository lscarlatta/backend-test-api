package com.backend.test.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.backend.test.model.Transaction;
import com.backend.test.service.TransactionService;
import spark.Route;

import java.util.List;

import static org.eclipse.jetty.http.HttpStatus.NO_CONTENT_204;
import static org.eclipse.jetty.http.HttpStatus.OK_200;

public class AccountApi extends BaseApi {

    public static Route findAllTransactionsById(TransactionService transactionService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            List<Transaction> transactions = transactionService.findAllByAccountId(id);
            JsonElement jsonResponse = new Gson().toJsonTree(transactions);

            return createResponse(transactions.isEmpty() ? NO_CONTENT_204 : OK_200, response, jsonResponse);
        };
    }
}
