package com.backend.test.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.backend.test.model.Account;
import com.backend.test.model.Bank;
import com.backend.test.service.AccountService;
import com.backend.test.service.BankService;
import spark.Route;

import java.util.List;

import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static org.eclipse.jetty.http.HttpStatus.NO_CONTENT_204;
import static org.eclipse.jetty.http.HttpStatus.OK_200;

public class BankApi extends BaseApi {

    public static Route create(BankService bankService) {
        return (request, response) -> {
            Bank bank = new Gson().fromJson(request.body(), Bank.class);
            bankService.create(bank);

            return createResponse(CREATED_201, response, null);
        };
    }

    public static Route findAll(BankService bankService) {
        return (request, response) -> {
            List<Bank> banks = bankService.findAll();
            JsonElement jsonResponse = new Gson().toJsonTree(banks);

            return createResponse(banks.isEmpty() ? NO_CONTENT_204 : OK_200, response, jsonResponse);
        };
    }

    public static Route update(BankService bankService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            Bank bank = new Gson().fromJson(request.body(), Bank.class);
            JsonElement jsonResponse = new Gson().toJsonTree(bankService.update(id, bank));

            return createResponse(OK_200, response, jsonResponse);
        };
    }

    public static Route findAllByBankId(AccountService accountService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            List<Account> accounts = accountService.findAllByBankId(id);
            JsonElement jsonResponse = new Gson().toJsonTree(accounts);

            return createResponse(accounts.isEmpty() ? NO_CONTENT_204 : OK_200, response, jsonResponse);
        };
    }

    public static Route createAccount(AccountService accountService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            Long userId = Long.parseLong(request.params(":userId"));

            Account account = new Gson().fromJson(request.body(), Account.class);
            accountService.create(account, id, userId);

            return createResponse(CREATED_201, response, null);
        };
    }
}
