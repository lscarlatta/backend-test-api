package com.backend.test.api;

import com.backend.test.model.response.StandardResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.backend.test.service.BaseService;
import spark.Response;
import spark.Route;

import static org.eclipse.jetty.http.HttpStatus.OK_200;

public abstract class BaseApi {

    static String createResponse(int status, Response response, JsonElement jsonResponse) {
        response.status(status);
        return new Gson().toJson(new StandardResponse(status, jsonResponse));
    }

    public static Route findById(BaseService baseService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            JsonElement jsonResponse = new Gson().toJsonTree(baseService.findById(id));

            return createResponse(OK_200, response, jsonResponse);
        };
    }
}
