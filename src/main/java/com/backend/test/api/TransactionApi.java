package com.backend.test.api;

import com.google.gson.Gson;
import com.backend.test.model.Transaction;
import com.backend.test.service.TransactionService;
import spark.Route;

import static org.eclipse.jetty.http.HttpStatus.CREATED_201;

public class TransactionApi extends BaseApi {

    public static Route create(TransactionService transactionService) {
        return (request, response) -> {
            Transaction transaction = new Gson().fromJson(request.body(), Transaction.class);
            transactionService.transaction(transaction);

            return createResponse(CREATED_201, response, null);
        };
    }
}
