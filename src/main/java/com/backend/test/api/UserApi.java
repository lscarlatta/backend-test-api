package com.backend.test.api;

import com.backend.test.service.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.backend.test.model.Account;
import com.backend.test.model.User;
import com.backend.test.service.AccountService;
import spark.Route;

import java.util.List;

import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static org.eclipse.jetty.http.HttpStatus.NO_CONTENT_204;
import static org.eclipse.jetty.http.HttpStatus.OK_200;

public class UserApi extends BaseApi {

    public static Route create(UserService userService) {
        return (request, response) -> {
            User user = new Gson().fromJson(request.body(), User.class);
            userService.create(user);

            return createResponse(CREATED_201, response, null);
        };
    }

    public static Route update(UserService userService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            User user = new Gson().fromJson(request.body(), User.class);
            JsonElement jsonResponse = new Gson().toJsonTree(userService.update(id, user));

            return createResponse(OK_200, response, jsonResponse);
        };
    }

    public static Route findAll(UserService userService) {
        return (request, response) -> {
            List<User> users = userService.findAll();
            JsonElement jsonResponse = new Gson().toJsonTree(users);

            return createResponse(users.isEmpty() ? NO_CONTENT_204 : OK_200, response, jsonResponse);
        };
    }

    public static Route findAllAccountsById(AccountService accountService) {
        return (request, response) -> {
            Long id = Long.parseLong(request.params(":id"));
            List<Account> accounts = accountService.findAllByUserId(id);
            JsonElement jsonResponse = new Gson().toJsonTree(accounts);

            return createResponse(accounts.isEmpty() ? NO_CONTENT_204 : OK_200, response, jsonResponse);
        };
    }
}
