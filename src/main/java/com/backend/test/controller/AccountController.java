package com.backend.test.controller;

import com.backend.test.api.AccountApi;
import com.backend.test.service.AccountService;
import com.backend.test.service.TransactionService;

import static spark.Spark.get;
import static spark.Spark.path;

public class AccountController {

    private AccountService accountService;
    private TransactionService transactionService;

    public AccountController(final AccountService accountService, final TransactionService transactionService) {
        this.accountService = accountService;
        this.transactionService = transactionService;
        resources();
    }

    private void resources() {
        path("/accounts", () -> {
            get("/:id", AccountApi.findById(accountService));
            get("/:id/transactions", AccountApi.findAllTransactionsById(transactionService));
        });
    }
}
