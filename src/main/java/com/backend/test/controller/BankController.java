package com.backend.test.controller;

import com.backend.test.api.BankApi;
import com.backend.test.service.AccountService;
import com.backend.test.service.BankService;

import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.post;
import static spark.Spark.put;

public class BankController {

    private BankService bankService;
    private AccountService accountService;

    public BankController(final BankService bankService, final AccountService accountService) {
        this.bankService = bankService;
        this.accountService = accountService;
        resources();
    }

    private void resources() {
        path("/banks", () -> {
            post("", BankApi.create(bankService));
            get("", BankApi.findAll(bankService));

            path("/:id", () -> {
                put("", BankApi.update(bankService));
                get("", BankApi.findById(bankService));
                get("/accounts", BankApi.findAllByBankId(accountService));
                post("/users/:userId/accounts", BankApi.createAccount(accountService));
            });
        });
    }
}
