package com.backend.test.controller;

import com.backend.test.api.BaseApi;
import com.backend.test.api.TransactionApi;
import com.backend.test.service.TransactionService;

import static spark.Spark.post;

public class TransactionController extends BaseApi {

    private TransactionService transactionService;

    public TransactionController(final TransactionService transactionService) {
        this.transactionService = transactionService;
        resources();
    }

    private void resources() {
        post("/transactions", TransactionApi.create(transactionService));
    }
}
