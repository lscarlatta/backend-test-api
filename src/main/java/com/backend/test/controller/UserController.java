package com.backend.test.controller;

import com.backend.test.api.BaseApi;
import com.backend.test.api.UserApi;
import com.backend.test.service.AccountService;
import com.backend.test.service.UserService;

import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.post;
import static spark.Spark.put;

public class UserController extends BaseApi {

    private UserService userService;
    private AccountService accountService;

    public UserController(final UserService userService, final AccountService accountService) {
        this.userService = userService;
        this.accountService = accountService;
        resources();
    }

    private void resources() {
        path("/users", () -> {
            post("", UserApi.create(userService));
            get("", UserApi.findAll(userService));

            path("/:id", () -> {
                put("", UserApi.update(userService));
                get("", UserApi.findById(userService));
                get("/accounts", UserApi.findAllAccountsById(accountService));
            });
        });
    }
}
