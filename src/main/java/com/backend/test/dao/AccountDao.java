package com.backend.test.dao;

import com.backend.test.model.Account;

import java.util.Collection;
import java.util.Optional;

public interface AccountDao {

    Optional<Account> findByIdNumber(Long idNumber);

    Account create(Account account);

    Account update(Long idNumber, Account account);

    Collection<Account> accounts();
}
