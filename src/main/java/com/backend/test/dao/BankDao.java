package com.backend.test.dao;

import com.backend.test.model.Bank;

import java.util.Collection;
import java.util.Optional;

public interface BankDao {

    Optional<Bank> findByIdNumber(Long idNumber);

    Bank create(Bank user);

    Bank update(Long idNumber, Bank user);

    Collection<Bank> banks();
}
