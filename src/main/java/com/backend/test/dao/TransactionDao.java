package com.backend.test.dao;

import com.backend.test.model.Transaction;

import java.util.Collection;

public interface TransactionDao {

    Boolean create(Transaction transaction);

    Collection<Transaction> transactions();
}
