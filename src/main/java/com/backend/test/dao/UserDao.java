package com.backend.test.dao;

import com.backend.test.model.User;

import java.util.Collection;
import java.util.Optional;

public interface UserDao {

    Optional<User> findByIdNumber(Long idNumber);

    User create(User user);

    User update(Long idNumber, User user);

    Collection<User> users();
}
