package com.backend.test.dao.impl;

import com.backend.test.exception.ExistException;
import com.backend.test.exception.NotFoundException;
import com.backend.test.model.Account;
import com.backend.test.dao.AccountDao;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class AccountDaoImpl implements AccountDao {

    private final static Logger LOGGER = Logger.getLogger(AccountDaoImpl.class);

    private static AccountDaoImpl instance = null;
    private final ConcurrentMap<Long, Account> accountMap;

    private AccountDaoImpl() {
        this.accountMap = new ConcurrentHashMap<>();
    }

    public static AccountDaoImpl newInstance() {
        if (instance == null) {
            instance = new AccountDaoImpl();
        }

        return instance;
    }

    @Override
    public Optional<Account> findByIdNumber(Long idNumber) {
        LOGGER.info(String.format("Find account by id. [Id: %s]", idNumber));

        return Optional.ofNullable(accountMap.get(idNumber));
    }

    @Override
    public Account create(Account account) {
        if (null != accountMap.putIfAbsent(account.getNumber(), account)) {
            String message = String.format("Account %s already exists", account.getNumber());
            LOGGER.error(message);
            throw new ExistException(message);
        }

        return account;
    }

    @Override
    public Account update(Long idNumber, Account account) {
        if (null != accountMap.replace(idNumber, account)) {
            return accountMap.get(idNumber);
        } else {
            String message = String.format("Account %s not found", idNumber);
            LOGGER.error(message);
            throw new NotFoundException(message);
        }
    }

    @Override
    public Collection<Account> accounts() {
        return accountMap.values();
    }
}
