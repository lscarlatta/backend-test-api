package com.backend.test.dao.impl;

import com.backend.test.exception.ExistException;
import com.backend.test.dao.BankDao;
import com.backend.test.exception.NotFoundException;
import com.backend.test.model.Bank;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class BankDaoImpl implements BankDao {

    private static final Logger LOGGER = Logger.getLogger(BankDaoImpl.class);

    private static BankDaoImpl instance = null;
    private final ConcurrentMap<Long, Bank> bankMap;

    private BankDaoImpl() {
        this.bankMap = new ConcurrentHashMap<>();
    }

    public static BankDaoImpl newInstance() {
        if (instance == null) {
            instance = new BankDaoImpl();
        }

        return instance;
    }

    @Override
    public Optional<Bank> findByIdNumber(Long idNumber) {
        LOGGER.info(String.format("Find bank by id. [Id: %s]", idNumber));

        return Optional.ofNullable(bankMap.get(idNumber));
    }

    @Override
    public Bank create(Bank bank) {
        if (null != bankMap.putIfAbsent(bank.getIdNumber(), bank)) {
            String message = String.format("Bank %s already exists", bank.getIdNumber());
            LOGGER.error(message);
            throw new ExistException(message);
        }

        return bank;
    }

    @Override
    public Bank update(Long idNumber, Bank bank) {
        if (null != bankMap.replace(idNumber, bank)) {
            return bankMap.get(idNumber);
        } else {
            String message = String.format("Bank %s not found", idNumber);
            LOGGER.error(message);
            throw new NotFoundException(message);
        }
    }

    @Override
    public Collection<Bank> banks() {
        return bankMap.values();
    }
}
