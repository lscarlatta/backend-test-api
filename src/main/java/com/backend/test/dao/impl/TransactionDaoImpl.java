package com.backend.test.dao.impl;

import com.backend.test.dao.TransactionDao;
import com.backend.test.model.Transaction;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

public class TransactionDaoImpl implements TransactionDao {

    private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

    private static TransactionDaoImpl instance = null;
    private final CopyOnWriteArrayList<Transaction> transactionMap;

    private TransactionDaoImpl() {
        this.transactionMap = new CopyOnWriteArrayList<>();
    }

    public static TransactionDaoImpl newInstance() {
        if (instance == null) {
            instance = new TransactionDaoImpl();
        }

        return instance;
    }

    @Override
    public Boolean create(Transaction transaction) {
        LOGGER.info("Create new transaction");

        return transactionMap.add(transaction);
    }

    @Override
    public Collection<Transaction> transactions() {
        return transactionMap;
    }
}
