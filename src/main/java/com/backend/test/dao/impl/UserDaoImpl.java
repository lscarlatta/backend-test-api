package com.backend.test.dao.impl;

import com.backend.test.exception.ExistException;
import com.backend.test.exception.NotFoundException;
import com.backend.test.dao.UserDao;
import com.backend.test.model.User;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);

    private static UserDaoImpl instance = null;
    private final ConcurrentMap<Long, User> userMap;

    private UserDaoImpl() {
        this.userMap = new ConcurrentHashMap<>();
    }

    public static UserDaoImpl newInstance() {
        if (instance == null) {
            instance = new UserDaoImpl();
        }

        return instance;
    }

    @Override
    public Optional<User> findByIdNumber(Long idNumber) {
        LOGGER.info(String.format("Find User by id. [Id: %s]", idNumber));

        return Optional.ofNullable(userMap.get(idNumber));
    }

    @Override
    public User create(User user) {
        if (null != userMap.putIfAbsent(user.getIdNumber(), user)) {
            String message = String.format("User %s already exists", user.getIdNumber());
            LOGGER.error(message);
            throw new ExistException(message);
        }

        return user;
    }

    @Override
    public User update(Long idNumber, User user) {
        if (null != userMap.replace(idNumber, user)) {
            return userMap.get(idNumber);
        } else {
            String message = String.format("User %s not found", idNumber);
            LOGGER.error(message);
            throw new NotFoundException(message);
        }
    }

    @Override
    public Collection<User> users() {
        return userMap.values();
    }
}
