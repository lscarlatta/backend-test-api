package com.backend.test.exception;

import com.backend.test.model.response.Error;
import com.backend.test.model.response.ErrorResponse;
import com.google.gson.Gson;
import spark.Request;
import spark.Response;

import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.INTERNAL_SERVER_ERROR_500;
import static org.eclipse.jetty.http.HttpStatus.NOT_FOUND_404;
import static spark.Spark.exception;
import static spark.Spark.internalServerError;
import static spark.Spark.notFound;

public class ExceptionHandler {

    public ExceptionHandler() {
        resources();
    }

    private void resources() {
        exception(NotFoundException.class, (Exception exception, Request request, Response response) -> {
            createResponse(exception, response, NOT_FOUND_404);
        });

        exception(TransactionNotAllowedException.class, (Exception exception, Request request, Response response) ->
                createResponse(exception, response, BAD_REQUEST_400));

        exception(ExistException.class, (Exception exception, Request request, Response response) ->
                createResponse(exception, response, BAD_REQUEST_400)
        );

        internalServerError((request, response) ->
                new Gson().toJson(new ErrorResponse(new Error(INTERNAL_SERVER_ERROR_500, "Internal server " +
                        "error")))
        );

        notFound((request, response) ->
                new Gson().toJson(new ErrorResponse(new Error(NOT_FOUND_404, "Resources not found")))
        );
    }

    private void createResponse(Exception exception, Response response, int notFound404) {
        response.status(notFound404);
        response.body(new Gson().toJson(new ErrorResponse(new Error(notFound404, exception.getMessage()))));
    }
}
