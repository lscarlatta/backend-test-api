package com.backend.test.model;

public class Account extends BaseModel {

    private Long number;
    private String name;
    private Status status;
    private Currency currency;
    private AccountType type;
    private Double balance;

    private Bank bank;
    private User user;

    public Account() {
        this.balance = 0D;
        this.currency = Currency.POUNDS_STERLING;
        this.status = Status.ACTIVE;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean isActive() {
        return Status.ACTIVE.equals(this.status);
    }
}
