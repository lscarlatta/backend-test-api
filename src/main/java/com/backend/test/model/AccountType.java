package com.backend.test.model;

public enum AccountType {
    CURRENT_ACCOUNT,
    SAVINGS_ACCOUNT
}
