package com.backend.test.model;

public enum Currency {
    POUNDS_STERLING("Pounds Sterling", 1.00),
    EURO("Euro", 1.15),
    DOLLAR("Dollar", 1.32);

    private String name;
    private Double value;

    /**
     * Create new currency
     *
     * @param name currency name
     * @param value value with respect to 1 Pound.
     */
    Currency(String name, Double value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
