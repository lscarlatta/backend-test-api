package com.backend.test.model;

public enum Status {
    ACTIVE,
    INACTIVE
}
