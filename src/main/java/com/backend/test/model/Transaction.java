package com.backend.test.model;

import java.util.Date;

public class Transaction {

    private Long originId;
    private Long destinationId;
    private Double amount;
    private Currency originCurrency;
    private Currency destinationCurrency;
    private Date date;

    public Long getOriginId() {
        return originId;
    }

    public void setOriginId(Long originId) {
        this.originId = originId;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getOriginCurrency() {
        return originCurrency;
    }

    public void setOriginCurrency(Currency originCurrency) {
        this.originCurrency = originCurrency;
    }

    public void setDestinationCurrency(Currency currency) {
        this.destinationCurrency = currency;
    }

    public Currency getDestinationCurrency() {
        return destinationCurrency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
