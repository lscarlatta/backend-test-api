package com.backend.test.model.builder;

import com.backend.test.model.Account;
import com.backend.test.model.AccountType;
import com.backend.test.model.Currency;
import com.backend.test.model.Status;
import com.backend.test.model.Bank;
import com.backend.test.model.User;

public class AccountBuilder {

    private static Account account;

    private AccountBuilder() { }

    public static AccountBuilder builder() {
        account = new Account();

        return new AccountBuilder();
    }

    public AccountBuilder withNumber(Long number) {
        account.setNumber(number);
        return this;
    }

    public AccountBuilder withStatus(Status status) {
        account.setStatus(status);
        return this;
    }

    public AccountBuilder withBalance(Double balance) {
        account.setBalance(balance);
        return this;
    }

    public AccountBuilder withBank(Bank bank) {
        account.setBank(bank);
        return this;
    }

    public AccountBuilder withUser(User user) {
        account.setUser(user);
        return this;
    }

    public AccountBuilder withCurrency(Currency currency) {
        account.setCurrency(currency);
        return this;
    }

    public AccountBuilder withName(String name) {
        account.setName(name);
        return this;
    }

    public AccountBuilder withType(AccountType type) {
        account.setType(type);
        return this;
    }

    public Account build() {
        return account;
    }
}
