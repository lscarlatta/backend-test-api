package com.backend.test.model.response;

public class ErrorResponse {
    private com.backend.test.model.response.Error error;

    public ErrorResponse(com.backend.test.model.response.Error error) {
        this.error = error;
    }

    public com.backend.test.model.response.Error getError() {
        return error;
    }

    public void setError(com.backend.test.model.response.Error error) {
        this.error = error;
    }
}
