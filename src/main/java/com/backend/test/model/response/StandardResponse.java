package com.backend.test.model.response;

import com.google.gson.JsonElement;

public class StandardResponse {

    private int status;
    private String message;
    private JsonElement data;

    public StandardResponse(int status, JsonElement data) {
        this.status = status;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }
}
