package com.backend.test.service;

import com.backend.test.model.Account;

import java.util.List;

public interface AccountService extends BaseService<Account> {

    void setUserService(UserService userService);

    void setBankService(BankService bankService);

    /**
     * Create Account.
     *
     * @param account Account to create
     * @return The new Account
     */
    Account create(Account account, Long bankId, Long userId);

    /**
     * Rollback Account by number.
     *
     * @param number  number to identify the Account to rollback
     * @param account The Account information to rollback.
     */
    void rollback(Long number, Account account);

    /**
     * Get all accounts by user id.
     *
     * @param userId User Id to identify All account from the User.
     * @return All accounts by user id
     */
    List<Account> findAllByUserId(Long userId);

    /**
     * Get all accounts by bank id.
     *
     * @param bankId Bank Id to identify All account from the Bank.
     * @return All accounts by bank id
     */
    List<Account> findAllByBankId(Long bankId);
}
