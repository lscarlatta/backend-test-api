package com.backend.test.service;

import com.backend.test.model.Bank;

public interface BankService extends BaseService<Bank> {
}
