package com.backend.test.service;

import java.util.List;

public interface BaseService<T> {

    /**
     * Find Model by Id number.
     *
     * @param id Id to identify the Model
     * @return Get the Model
     */
    T findById(Long id);

    /**
     * Create Model.
     *
     * @param model Model to create
     * @return The new Model
     */
    T create(T model);

    /**
     * Update Model by Id number.
     *
     * @param id   Id to identify the Model
     * @param model The Model information.
     * @return The updated Model.
     */
    T update(Long id, T model);

    /**
     * Get all models.
     *
     * @return All models
     */
    List<T> findAll();
}
