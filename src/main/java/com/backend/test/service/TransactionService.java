package com.backend.test.service;

import com.backend.test.model.Transaction;

import java.util.List;

public interface TransactionService {

    void setAccountService(AccountService accountService);

    /**
     * Create new transaction
     *
     * @param transaction Transaction to realize.
     */
    void transaction(Transaction transaction);

    /**
     * Get all transactions by account id.
     *
     * @param accountId Account Id to identify All transaction from the Account.
     * @return All Transaction by account id
     */
    List<Transaction> findAllByAccountId(Long accountId);
}
