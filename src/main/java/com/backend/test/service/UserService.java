package com.backend.test.service;

import com.backend.test.model.User;

public interface UserService extends BaseService<User> {
}
