package com.backend.test.service.impl;

import com.backend.test.service.UserService;
import com.backend.test.dao.AccountDao;
import com.backend.test.dao.impl.AccountDaoImpl;
import com.backend.test.exception.NotFoundException;
import com.backend.test.exception.NotImplementedException;
import com.backend.test.model.Account;
import com.backend.test.model.Bank;
import com.backend.test.model.User;
import com.backend.test.service.AccountService;
import com.backend.test.service.BankService;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AccountServiceImpl implements AccountService {

    private AccountDao accountDao;
    private UserService userService;
    private BankService bankService;

    public AccountServiceImpl() {
        this.accountDao = AccountDaoImpl.newInstance();
    }

    @Override
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void setBankService(BankService bankService) {
        this.bankService = bankService;
    }

    @Override
    public Account findById(Long id) {
        return accountDao.findByIdNumber(id).orElseThrow(() -> new NotFoundException(String.format("Account %s not " +
                "found", id)));
    }

    @Override
    public Account create(Account model) {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    public Account update(Long id, Account model) {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    public List<Account> findAll() {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    public Account create(Account account, Long bankId, Long userId) {
        Bank bank = bankService.findById(bankId);
        User user = userService.findById(userId);

        account.setBank(bank);
        account.setUser(user);
        account.setCreatedAt(new Date());

        return accountDao.create(account);
    }

    @Override
    public void rollback(Long number, Account account) {
        accountDao.update(number, account);
    }

    @Override
    public List<Account> findAllByUserId(Long userId) {
        return accountDao.accounts()
                .stream().filter(account -> Objects.equals(account.getUser().getIdNumber(), userId))
                .sorted(Comparator.comparing(Account::getNumber))
                .collect(Collectors.toList());
    }

    @Override
    public List<Account> findAllByBankId(Long bankId) {
        return accountDao.accounts()
                .stream().filter(account -> Objects.equals(account.getBank().getIdNumber(), bankId))
                .sorted(Comparator.comparing(Account::getNumber))
                .collect(Collectors.toList());
    }
}
