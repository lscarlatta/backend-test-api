package com.backend.test.service.impl;

import com.backend.test.dao.impl.BankDaoImpl;
import com.backend.test.exception.NotFoundException;
import com.backend.test.dao.BankDao;
import com.backend.test.model.Bank;
import com.backend.test.service.BankService;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class BankServiceImpl implements BankService {

    private BankDao bankDao;

    public BankServiceImpl() {
        this.bankDao = BankDaoImpl.newInstance();
    }

    @Override
    public Bank findById(Long id) {
        return bankDao.findByIdNumber(id).orElseThrow(() -> new NotFoundException(String.format("Bank %s not found",
                id)));
    }

    @Override
    public Bank create(Bank bank) {
        bank.setCreatedAt(new Date());

        return bankDao.create(bank);
    }

    @Override
    public Bank update(Long id, Bank update) {
        Bank bank = findById(id);
        updateBank(bank, update);

        return bankDao.update(id, bank);
    }

    @Override
    public List<Bank> findAll() {
        return bankDao.banks().stream().sorted(Comparator.comparing(Bank::getIdNumber)).collect(Collectors.toList());
    }

    private void updateBank(Bank user, Bank update) {
        user.setUpdatedAt(new Date());
        user.setDescription(update.getDescription());
        user.setName(update.getName());
    }
}
