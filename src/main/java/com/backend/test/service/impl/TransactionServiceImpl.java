package com.backend.test.service.impl;

import com.backend.test.dao.TransactionDao;
import com.backend.test.dao.impl.TransactionDaoImpl;
import com.backend.test.exception.TransactionNotAllowedException;
import com.backend.test.model.Account;
import com.backend.test.model.Currency;
import com.backend.test.model.Transaction;
import com.backend.test.service.AccountService;
import com.backend.test.service.TransactionService;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TransactionServiceImpl implements TransactionService {

    private static final Logger LOGGER = Logger.getLogger(TransactionServiceImpl.class);

    private TransactionDao transactionDao;
    private AccountService accountService;

    public TransactionServiceImpl() {
        this.transactionDao = TransactionDaoImpl.newInstance();
    }

    @Override
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void transaction(Transaction transaction) {
        LOGGER.info(String.format("Create transaction between Account %s and Account %s", transaction.getOriginId(),
                transaction.getDestinationId()));
        Account origin = accountService.findById(transaction.getOriginId());
        Account destination = accountService.findById(transaction.getDestinationId());

        try {
            createTransaction(origin, destination, transaction.getAmount());
            transaction.setOriginCurrency(origin.getCurrency());
            transaction.setDestinationCurrency(destination.getCurrency());
            transaction.setDate(new Date());

            transactionDao.create(transaction);
        } catch (TransactionNotAllowedException e) {
            rollbackTransaction(origin, destination);
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public List<Transaction> findAllByAccountId(Long accountId) {
        return transactionDao.transactions().stream().filter(transaction ->
                Objects.equals(transaction.getOriginId(), accountId) || Objects.equals(transaction.getDestinationId(),
                        accountId)).collect(Collectors.toList());
    }

    private void rollbackTransaction(Account origin, Account destination) {
        accountService.rollback(origin.getNumber(), origin);
        accountService.rollback(destination.getNumber(), destination);
    }

    private void createTransaction(Account origin, Account destination, Double money) {
        Double originNewBalance = origin.getBalance() - money;
        if (originNewBalance < 0) {
            String message = String.format("The account %s does not have enough money", origin.getNumber());
            throw new TransactionNotAllowedException(message);
        }
        origin.setBalance(originNewBalance);

        Double convert = convertDestinationMoney(money, origin.getCurrency(), destination.getCurrency());

        Double destinationNewBalance = destination.getBalance() + convert;
        destination.setBalance(destinationNewBalance);
    }

    private Double convertDestinationMoney(Double money, Currency originCurrency, Currency destinationCurrency) {
        return money * destinationCurrency.getValue() / originCurrency.getValue();
    }


}
