package com.backend.test.service.impl;

import com.backend.test.dao.impl.UserDaoImpl;
import com.backend.test.exception.NotFoundException;
import com.backend.test.service.UserService;
import com.backend.test.dao.UserDao;
import com.backend.test.model.User;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl() {
        this.userDao = UserDaoImpl.newInstance();
    }

    @Override
    public User findById(Long id) {
        return userDao.findByIdNumber(id).orElseThrow(() -> new NotFoundException(String.format("User %s not found",
                id)));
    }

    @Override
    public User create(User user) {
        user.setCreatedAt(new Date());

        return userDao.create(user);
    }

    @Override
    public User update(Long id, User update) {
        User user = findById(id);

        updateUser(user, update);
        user.setUpdatedAt(new Date());

        return userDao.update(id, user);
    }

    @Override
    public List<User> findAll() {
        return userDao.users()
                .stream()
                .sorted(Comparator.comparing(User::getIdNumber))
                .collect(Collectors.toList());
    }

    private void updateUser(User user, User update) {
        user.setEmail(update.getEmail());
        user.setFirstName(update.getFirstName());
        user.setLastName(update.getLastName());
    }
}
