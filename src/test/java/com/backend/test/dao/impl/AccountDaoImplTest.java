package com.backend.test.dao.impl;

import com.backend.test.exception.ExistException;
import com.backend.test.exception.NotFoundException;
import com.backend.test.model.Account;
import com.backend.test.model.AccountType;
import com.backend.test.model.Currency;
import com.backend.test.model.Status;
import com.backend.test.model.builder.AccountBuilder;
import com.backend.test.model.Bank;
import com.backend.test.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class AccountDaoImplTest {

    @InjectMocks
    private AccountDaoImpl dao;

    @Before
    public void setUp() {
        Long number = 1111L;
        Long userId = 123L;
        Long bankId = 890L;
        Double balance = 100D;
        Currency currency = Currency.POUNDS_STERLING;
        String name = "Test";
        Status status = Status.ACTIVE;
        AccountType type = AccountType.CURRENT_ACCOUNT;

        Bank bank = new Bank();
        bank.setIdNumber(bankId);

        User user = new User();
        user.setIdNumber(userId);

        AccountBuilder builder = AccountBuilder.builder();
        Account account = builder.withBalance(balance).withBank(bank).withCurrency(currency).withName(name)
                .withNumber(number).withStatus(status).withType(type).withUser(user).build();

        dao.create(account);
    }

    @Test
    public void findByIdNumberTest() {
        Long number = 123L;

        Account account = new Account();
        account.setNumber(number);

        Optional<Account> result = dao.findByIdNumber(number);

        assertThat(result, is(not(nullValue())));
    }

    @Test
    public void createTest() {
        Long number = 1234L;

        Account account = new Account();
        account.setNumber(number);

        Account result = dao.create(account);

        assertThat(result, is(not(nullValue())));
    }

    @Test
    public void createAlreadyExistTest() {
        Long number = 1111L;

        Account account = new Account();
        account.setNumber(number);

        try {
            dao.create(account);
            fail();
        } catch (ExistException e) {
            assertThat(e.getMessage(), is(String.format("Account %s already exists", account.getNumber())));
        }
    }

    @Test
    public void updateTest() {
        Long number = 1111L;
        Long userId = 1234L;
        Long bankId = 7890L;
        Double balance = 50D;
        Currency currency = Currency.POUNDS_STERLING;
        String name = "Other";
        Status status = Status.INACTIVE;
        AccountType type = AccountType.SAVINGS_ACCOUNT;

        Bank bank = new Bank();
        bank.setIdNumber(bankId);

        User user = new User();
        user.setIdNumber(userId);

        AccountBuilder builder = AccountBuilder.builder();
        Account account = builder.withBalance(balance).withBank(bank).withCurrency(currency).withName(name)
                .withNumber(number).withStatus(status).withType(type).withUser(user).build();

        Account result = dao.update(number, account);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getNumber(), is(number));
        assertThat(result.getBalance(), is(balance));
        assertThat(result.getBank(), is(bank));
        assertThat(result.getCurrency(), is(currency));
        assertThat(result.getName(), is(name));
        assertThat(result.getType(), is(type));
        assertThat(result.getUser(), is(user));
        assertThat(result.getStatus(), is(status));
    }

    @Test
    public void updateNotExistTest() {
        Long number = 1234L;

        Account account = new Account();
        account.setNumber(number);

        try {
            dao.update(number, account);
            fail();
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is(String.format("Account %s not found", number)));
        }
    }

    @Test
    public void findAllTest() {
        Collection<Account> accounts = dao.accounts();

        assertThat(accounts.isEmpty(), is(false));
    }
}
