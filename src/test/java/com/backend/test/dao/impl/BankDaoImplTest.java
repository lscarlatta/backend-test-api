package com.backend.test.dao.impl;

import com.backend.test.exception.ExistException;
import com.backend.test.exception.NotFoundException;
import com.backend.test.model.Bank;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class BankDaoImplTest {

    @InjectMocks
    private BankDaoImpl dao;

    @Before
    public void setUp() {
        Bank bank = new Bank();
        bank.setIdNumber(123L);
        bank.setName("Test");
        bank.setDescription("Mock");

        dao.create(bank);
    }

    @Test
    public void findByIdNumberTest() {
        Long idNumber = 123L;

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);

        Optional<Bank> result = dao.findByIdNumber(idNumber);

        assertThat(result, is(not(nullValue())));
    }

    @Test
    public void createTest() {
        Long idNumber = 1234L;

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);

        Bank result = dao.create(bank);

        assertThat(result, is(not(nullValue())));
    }

    @Test
    public void createAlreadyExistTest() {
        Long idNumber = 123L;

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);

        try {
            dao.create(bank);
            fail();
        } catch (ExistException e) {
            assertThat(e.getMessage(), is(String.format("Bank %s already exists", bank.getIdNumber())));
        }
    }

    @Test
    public void updateTest() {
        Long idNumber = 123L;
        String name = "Other";
        String description = "Thing";

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);
        bank.setName(name);
        bank.setDescription(description);

        Bank result = dao.update(idNumber, bank);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getName(), is(name));
        assertThat(result.getDescription(), is(description));
    }

    @Test
    public void updateNotExistTest() {
        Long idNumber = 1234L;

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);

        try {
            dao.update(idNumber, bank);
            fail();
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is(String.format("Bank %s not found", idNumber)));
        }
    }

    @Test
    public void findAllTest() {
        Collection<Bank> banks = dao.banks();

        assertThat(banks.isEmpty(), is(false));
    }
}
