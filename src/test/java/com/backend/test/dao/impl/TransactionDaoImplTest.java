package com.backend.test.dao.impl;

import com.backend.test.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TransactionDaoImplTest {

    @InjectMocks
    private TransactionDaoImpl dao;

    @Test
    public void createTest() {
        Transaction transaction = new Transaction();
        transaction.setAmount(100D);
        transaction.setDestinationId(1111L);
        transaction.setOriginId(2222L);

        Boolean result = dao.create(transaction);

        assertThat(result, is(true));
    }

    @Test
    public void findAllTest() {
        Transaction transaction = new Transaction();
        transaction.setAmount(100D);
        transaction.setDestinationId(1111L);
        transaction.setOriginId(2222L);

        dao.create(transaction);

        Collection<Transaction> transactions = dao.transactions();

        assertThat(transactions.isEmpty(), is(false));
    }
}
