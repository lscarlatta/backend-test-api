package com.backend.test.dao.impl;

import com.backend.test.exception.ExistException;
import com.backend.test.exception.NotFoundException;
import com.backend.test.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoImplTest {

    @InjectMocks
    private UserDaoImpl dao;

    @Before
    public void setUp() {
        User user = new User();
        user.setIdNumber(123L);
        user.setFirstName("Test");
        user.setLastName("Mock");

        dao.create(user);
    }

    @Test
    public void findByIdNumberTest() {
        Long idNumber = 123L;

        User user = new User();
        user.setIdNumber(idNumber);

        Optional<User> result = dao.findByIdNumber(idNumber);

        assertThat(result, is(not(nullValue())));
    }

    @Test
    public void createTest() {
        Long idNumber = 1234L;

        User user = new User();
        user.setIdNumber(idNumber);

        User result = dao.create(user);

        assertThat(result, is(not(nullValue())));
    }

    @Test
    public void createAlreadyExistTest() {
        Long idNumber = 123L;

        User user = new User();
        user.setIdNumber(idNumber);

        try {
            dao.create(user);
            fail();
        } catch (ExistException e) {
            assertThat(e.getMessage(), is(String.format("User %s already exists", user.getIdNumber())));
        }
    }

    @Test
    public void updateTest() {
        Long idNumber = 123L;
        String firstName = "Other";
        String lastName = "Thing";

        User user = new User();
        user.setIdNumber(idNumber);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        User result = dao.update(idNumber, user);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getFirstName(), is(firstName));
        assertThat(result.getLastName(), is(lastName));
    }

    @Test
    public void updateNotExistTest() {
        Long idNumber = 1234L;

        User user = new User();
        user.setIdNumber(idNumber);

        try {
            dao.update(idNumber, user);
            fail();
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is(String.format("User %s not found", idNumber)));
        }
    }

    @Test
    public void findAllTest() {
        Collection<User> users = dao.users();

        assertThat(users.isEmpty(), is(false));
    }
}
