package com.backend.test.service.impl;

import com.backend.test.exception.NotImplementedException;
import com.backend.test.model.Account;
import com.backend.test.model.AccountType;
import com.backend.test.model.Currency;
import com.backend.test.model.Status;
import com.backend.test.service.BankService;
import com.backend.test.service.UserService;
import com.backend.test.dao.AccountDao;
import com.backend.test.model.Bank;
import com.backend.test.model.User;
import com.backend.test.model.builder.AccountBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    @Mock
    private AccountDao dao;

    @Mock
    private UserService userService;

    @Mock
    private BankService bankService;

    @InjectMocks
    private AccountServiceImpl service;

    @Test
    public void findByIdTest() {
        Long number = 123L;
        Account account = new Account();
        account.setNumber(number);

        when(dao.findByIdNumber(number)).thenReturn(Optional.of(account));

        Account result = service.findById(number);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getNumber(), is(number));
    }

    @Test
    public void createNotImplementedExceptionTest() {
        try {
            service.create(new Account());
            fail();
        } catch (NotImplementedException e) {
            assertThat(e.getMessage(), is("Not implemented"));
        }
    }

    @Test
    public void updateNotImplementedExceptionTest() {
        try {
            service.update(1L, new Account());
            fail();
        } catch (NotImplementedException e) {
            assertThat(e.getMessage(), is("Not implemented"));
        }
    }

    @Test
    public void findAllNotImplementedExceptionTest() {
        try {
            service.findAll();
            fail();
        } catch (NotImplementedException e) {
            assertThat(e.getMessage(), is("Not implemented"));
        }
    }

    @Test
    public void createTest() {
        Long number = 1111L;
        Long userId = 123L;
        Long bankId = 890L;
        Double balance = 100D;
        Currency currency = Currency.POUNDS_STERLING;
        String name = "Test";
        Status status = Status.ACTIVE;
        AccountType type = AccountType.CURRENT_ACCOUNT;

        Bank bank = new Bank();
        bank.setIdNumber(bankId);

        User user = new User();
        user.setIdNumber(userId);

        AccountBuilder builder = AccountBuilder.builder();
        Account account = builder.withBalance(balance).withBank(bank).withCurrency(currency).withName(name)
                .withNumber(number).withStatus(status).withType(type).withUser(user).build();

        when(bankService.findById(bankId)).thenReturn(bank);
        when(userService.findById(userId)).thenReturn(user);
        when(dao.create(any(Account.class))).thenReturn(account);

        Account result = service.create(account, bankId, userId);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getNumber(), is(number));
        assertThat(result.getBalance(), is(balance));
        assertThat(result.getBank(), is(bank));
        assertThat(result.getCurrency(), is(currency));
        assertThat(result.getName(), is(name));
        assertThat(result.getType(), is(type));
        assertThat(result.getUser(), is(user));
        assertThat(result.getStatus(), is(status));
        assertThat(result.isActive(), is(true));
    }

    @Test
    public void rollbackTest() {
        Long number = 1111L;

        AccountBuilder builder = AccountBuilder.builder();
        Account account = builder.withNumber(number).build();

        service.rollback(number, account);

        verify(dao, times(1)).update(number, account);
    }

    @Test
    public void findAllByUserIdTest() {
        Long number = 1111L;
        Long userId = 123L;
        Long otherUserId = 890L;

        Account account = createAccountByUserId(number, userId);
        Account otherAccount = createAccountByUserId(number, otherUserId);

        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        accounts.add(otherAccount);

        when(dao.accounts()).thenReturn(accounts);

        List<Account> result = service.findAllByUserId(userId);

        assertThat(result.isEmpty(), is(false));
        assertThat(result.size(), is(1));
    }

    @Test
    public void findAllByBankIdTest() {
        Long number = 1111L;
        Long bankId = 123L;
        Long otherBankId = 789L;

        Account account = createAccountByBankId(number, bankId);
        Account otherAccount = createAccountByBankId(number, otherBankId);

        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        accounts.add(otherAccount);

        when(dao.accounts()).thenReturn(accounts);

        List<Account> result = service.findAllByBankId(bankId);

        assertThat(result.isEmpty(), is(false));
        assertThat(result.size(), is(1));
    }

    private Account createAccountByUserId(Long number, Long userId) {
        User user = new User();
        user.setIdNumber(userId);

        AccountBuilder builder = AccountBuilder.builder();
        return builder.withNumber(number).withUser(user).build();
    }

    private Account createAccountByBankId(Long number, Long bankId) {
        Bank bank = new Bank();
        bank.setIdNumber(bankId);

        AccountBuilder builder = AccountBuilder.builder();
        return builder.withNumber(number).withBank(bank).build();
    }
}
