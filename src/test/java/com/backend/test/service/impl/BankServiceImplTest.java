package com.backend.test.service.impl;

import com.backend.test.exception.NotFoundException;
import com.backend.test.dao.BankDao;
import com.backend.test.model.Bank;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BankServiceImplTest {

    @Mock
    private BankDao dao;

    @InjectMocks
    private BankServiceImpl service;

    @Test
    public void findByIdTest() {
        Long idNumber = 123L;
        Bank bank = new Bank();
        bank.setIdNumber(idNumber);

        when(dao.findByIdNumber(idNumber)).thenReturn(Optional.of(bank));

        Bank result = service.findById(idNumber);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getIdNumber(), is(idNumber));
    }

    @Test
    public void findByIdNotFoundTest() {
        Long idNumber = 123L;
        when(dao.findByIdNumber(idNumber)).thenReturn(Optional.empty());

        try {
            service.findById(idNumber);
            fail();
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is(String.format("Bank %s not found", idNumber)));
        }
    }

    @Test
    public void createTest() {
        Long idNumber = 123L;
        String name = "test";
        String description = "mock";

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);
        bank.setName(name);
        bank.setDescription(description);

        when(dao.create(any(Bank.class))).thenReturn(bank);

        Bank result = service.create(bank);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getIdNumber(), is(idNumber));
        assertThat(result.getName(), is(name));
        assertThat(result.getDescription(), is(description));
    }

    @Test
    public void updateTest() {
        Long idNumber = 123L;
        String name = "test";
        String description = "mock";

        Bank old = new Bank();
        old.setIdNumber(idNumber);

        Bank bank = new Bank();
        bank.setIdNumber(idNumber);
        bank.setName(name);
        bank.setDescription(description);

        when(dao.findByIdNumber(idNumber)).thenReturn(Optional.of(old));
        when(dao.update(any(Long.class), any(Bank.class))).thenReturn(bank);

        Bank result = service.update(idNumber, bank);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getIdNumber(), is(idNumber));
        assertThat(result.getName(), is(name));
        assertThat(result.getDescription(), is(description));
    }

    @Test
    public void findAllTest() {
        Bank bank = new Bank();
        bank.setIdNumber(1234L);

        List<Bank> banks = new ArrayList<>();
        banks.add(bank);

        when(dao.banks()).thenReturn(banks);

        List<Bank> result = service.findAll();

        assertThat(result.isEmpty(), is(false));
        assertThat(result, is(banks));
    }
}
