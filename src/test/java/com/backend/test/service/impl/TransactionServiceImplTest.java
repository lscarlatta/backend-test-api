package com.backend.test.service.impl;

import com.backend.test.dao.TransactionDao;
import com.backend.test.exception.TransactionNotAllowedException;
import com.backend.test.model.Account;
import com.backend.test.model.Transaction;
import com.backend.test.service.AccountService;
import com.backend.test.model.builder.AccountBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTest {

    @Mock
    private TransactionDao dao;

    @Mock
    private AccountService accountService;

    @InjectMocks
    private TransactionServiceImpl service;

    @Test
    public void transactionTest() {
        Double amount = 100D;
        Long originId = 1111L;
        Long destinationId = 2222L;

        Account origin = createAccount(originId);
        Account destination = createAccount(destinationId);

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setOriginId(originId);
        transaction.setDestinationId(destinationId);

        when(accountService.findById(originId)).thenReturn(origin);
        when(accountService.findById(destinationId)).thenReturn(destination);
        when(dao.create(any(Transaction.class))).thenReturn(true);

        service.transaction(transaction);

        verify(dao, times(1)).create(any(Transaction.class));
        verify(accountService, times(0)).rollback(any(Long.class), any(Account.class));
    }

    @Test
    public void transactionFailTest() {
        Double amount = 50000D;
        Long originId = 1111L;
        Long destinationId = 2222L;

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setOriginId(originId);
        transaction.setDestinationId(destinationId);

        Account origin = createAccount(originId);

        Account destination = createAccount(destinationId);

        when(accountService.findById(originId)).thenReturn(origin);
        when(accountService.findById(destinationId)).thenReturn(destination);

        try {
            service.transaction(transaction);
            fail();
        } catch (TransactionNotAllowedException e) {
            assertThat(e.getMessage(), is(String.format("The account %s does not have enough money", origin.getNumber
                    ())));
        }

        verify(dao, times(0)).create(any(Transaction.class));
        verify(accountService, times(2)).rollback(any(Long.class), any(Account.class));
    }

    @Test
    public void findAllByOriginAccountId() {
        Double amount = 50000D;
        Long originId = 1111L;
        Long destinationId = 2222L;

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setOriginId(originId);
        transaction.setDestinationId(destinationId);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        when(dao.transactions()).thenReturn(transactions);

        List<Transaction> result = service.findAllByAccountId(originId);

        assertThat(result.isEmpty(), is(false));
        assertThat(result, is(transactions));
    }

    @Test
    public void findAllByDestinationAccountId() {
        Double amount = 50000D;
        Long originId = 1111L;
        Long destinationId = 2222L;

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setOriginId(originId);
        transaction.setDestinationId(destinationId);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        when(dao.transactions()).thenReturn(transactions);

        List<Transaction> result = service.findAllByAccountId(destinationId);

        assertThat(result.isEmpty(), is(false));
        assertThat(result, is(transactions));
    }

    private Account createAccount(Long destinationId) {
        Double destinationBalance = 1000D;
        AccountBuilder destinationBuilder = AccountBuilder.builder();

        return destinationBuilder.withNumber(destinationId).withBalance(destinationBalance).build();
    }
}