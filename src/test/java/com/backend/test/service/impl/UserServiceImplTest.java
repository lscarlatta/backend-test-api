package com.backend.test.service.impl;

import com.backend.test.exception.NotFoundException;
import com.backend.test.dao.UserDao;
import com.backend.test.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserDao dao;

    @InjectMocks
    private UserServiceImpl service;

    @Test
    public void findByIdTest() {
        Long idNumber = 123L;
        User user = new User();
        user.setIdNumber(idNumber);

        when(dao.findByIdNumber(idNumber)).thenReturn(Optional.of(user));

        User result = service.findById(idNumber);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getIdNumber(), is(idNumber));
    }

    @Test
    public void findByIdNotFoundTest() {
        Long idNumber = 123L;
        when(dao.findByIdNumber(idNumber)).thenReturn(Optional.empty());

        try {
            service.findById(idNumber);
            fail();
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is(String.format("User %s not found", idNumber)));
        }
    }

    @Test
    public void createTest() {
        Long idNumber = 123L;
        String firstName = "test";
        String lastName = "mock";
        String email = "test@mock";

        User user = new User();
        user.setIdNumber(idNumber);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);

        when(dao.create(any(User.class))).thenReturn(user);

        User result = service.create(user);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getIdNumber(), is(idNumber));
        assertThat(result.getFirstName(), is(firstName));
        assertThat(result.getLastName(), is(lastName));
        assertThat(result.getEmail(), is(email));
    }

    @Test
    public void updateTest() {
        Long idNumber = 123L;
        String firstName = "test";
        String lastName = "mock";
        String email = "test@mock";

        User old = new User();
        old.setIdNumber(idNumber);

        User user = new User();
        user.setIdNumber(idNumber);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);

        when(dao.findByIdNumber(idNumber)).thenReturn(Optional.of(old));
        when(dao.update(any(Long.class), any(User.class))).thenReturn(user);

        User result = service.update(idNumber, user);

        assertThat(result, is(not(nullValue())));
        assertThat(result.getIdNumber(), is(idNumber));
        assertThat(result.getFirstName(), is(firstName));
        assertThat(result.getLastName(), is(lastName));
        assertThat(result.getEmail(), is(email));
    }

    @Test
    public void findAllTest() {
        User user = new User();
        user.setIdNumber(1234L);

        List<User> users = new ArrayList<>();
        users.add(user);

        when(dao.users()).thenReturn(users);

        List<User> result = service.findAll();

        assertThat(result.isEmpty(), is(false));
        assertThat(result, is(users));
    }
}
